/* Assignment 11. Two routers five subnets. */
version 12.1X47-D15.4;
system {
    host-name vSRX_1;
    /* User: root Password: Rootpass */
    root-authentication {
        encrypted-password "$1$4TkbZDtp$6E8C6Bg7K6gnHR31XnJjl0";
    }
}
interfaces {
    ge-0/0/1 {
        unit 0 {
            family inet {
                /* Lan1 */
                address 192.168.10.1/24;
            }
        }
    }
    ge-0/0/2 {
        unit 0 {
            family inet {
                /* Lan2 */
                address 192.168.11.1/24;
            }
        }
    }
    ge-0/0/3 {
        unit 0 {
            family inet {
                /* Lan5 */
                address 10.10.10.1/30;
            }
        }
    }
}
routing-options {
    static {
        /* Route to Lan3 */
        route 192.168.12.0/24 next-hop 10.10.10.2;
        /* Route to Lan4 */
        route 192.168.13.0/24 next-hop 10.10.10.2;
    }
}
security {
    policies {
        from-zone myTrust_1 to-zone myTrust_1 {
            policy default-permit {
                match {
                    source-address any;
                    destination-address any;
                    application any;
                }
                then {
                    permit;
                }
            }
        }
    }
    zones {
        security-zone myTrust_1 {
            interfaces {
                ge-0/0/1.0 {
                    host-inbound-traffic {
                        system-services {
                            ping;
                        }
                    }
                }
                ge-0/0/2.0 {
                    host-inbound-traffic {
                        system-services {
                            ping;
                        }
                    }
                }
                ge-0/0/3.0 {
                    host-inbound-traffic {
                        system-services {
                            ping;
                        }
                    }
                }
            }
        }
    }
}
