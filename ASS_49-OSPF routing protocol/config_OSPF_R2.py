## Assignment 49 OSPF routing protocol
/* By RVP. */
version 12.1X47-D15.4;
system {
    host-name R2;
    /* User: root Password: Rootpass */
    root-authentication {
        encrypted-password "$1$xH9xJoL6$MFOUYnZr4.Qj2NM24XInz/";
    }
}
interfaces {
    ge-0/0/1 {
        unit 0 {
            family inet {
                /* Lan10 */
                address 10.10.12.2/28;
            }
        }
    }
    ge-0/0/2 {
        unit 0 {
            family inet {
                /* Lan5 */
                address 10.10.10.1/28;
            }
        }
    }
}
protocols {
    ospf {
        area 0.0.0.0 {
            interface ge-0/0/1.0;
            interface ge-0/0/2.0;
        }
    }
}
security {
    policies {
        /* This policy is needed for interfaceses in the R5_trust zone to intercommunicate */
        from-zone R2_trust to-zone R2_trust {
            policy default-permit {
                match {
                    source-address any;
                    destination-address any;
                    application any;
                }
                then {
                    permit;
                }
            }
        }
    }
    zones {
        security-zone R2_trust {
            interfaces {
                ge-0/0/1.0 {
                    host-inbound-traffic {
                        system-services {
                            ping;
                        }
                        protocols {
            /* Open this interface up for participation in OSPF */
                            ospf;
                        }
                    }
                }
                ge-0/0/2.0 {
                    host-inbound-traffic {
                        system-services {
                            ping;
                        }
                        protocols {
            /* Open this interface up for participation in OSPF */
                            ospf;
                        }
                    }
                }
            }
        }
    }
}

